extends Node2D

var arreglo = []
var arreglo2 = []

func _ready():
	get_node("Elemento1/Polygon2D/Significado").set_bbcode("Conjunto de programas que han sido escritos para servir a otros programas")
	get_node("Elemento2/Polygon2D/Significado").set_bbcode("Coordina/analiza/controla sucesos del mundo real conforme ocurren")
	get_node("Elemento3/Polygon2D/Significado").set_bbcode("Restructuran los datos existentes para facilitar las operaciones o gestionar la toma de decisiones")
	get_node("Elemento4/Polygon2D/Significado").set_bbcode("Caracterizado por los algoritmos de <<manejo de números>>")
	get_node("Elemento5/Polygon2D/Significado").set_bbcode("Controlar productos y sistemas de los mercados industriales y de consumo")
	get_node("Elemento6/Polygon2D/Significado").set_bbcode("Conjunto de aplicaciones para procesamiento de texto, multimedia, entretenimiento, bases de datos, acceso a redes")
	get_node("Elemento7/Polygon2D/Significado").set_bbcode("Incorpora instrucciones ejecutables y datos, accedidos mediante un modem hacia una red que proporciona un recurso software")
	get_node("Elemento8/Polygon2D/Significado").set_bbcode("Uso de algoritmos no numéricos para resolver problemas para los que no son adecuados el cálculo o análisis directo")
	
	get_node("Elemento1/Sprite/Concepto").set_bbcode("De sistemas")
	get_node("Elemento2/Sprite/Concepto").set_bbcode("De tiempo real")
	get_node("Elemento3/Sprite/Concepto").set_bbcode("De gestión")
	get_node("Elemento4/Sprite/Concepto").set_bbcode("De ingeniería y científico")
	get_node("Elemento5/Sprite/Concepto").set_bbcode("Empotrado")
	get_node("Elemento6/Sprite/Concepto").set_bbcode("De computadores personales")
	get_node("Elemento7/Sprite/Concepto").set_bbcode("Basado en web")
	get_node("Elemento8/Sprite/Concepto").set_bbcode("De inteligencia artificial")
	
	for i in range(0, 7):
		arreglo.append(get_node("Elemento" + str(i+1) + "/Sprite").get_global_pos())
		arreglo2.append(get_node("Elemento" + str(i+1) + "/Polygon2D").get_global_pos())
	
	arreglo = shuffleList(arreglo)
	arreglo2 = shuffleList(arreglo2)
	
	for i in range(0, 7):
		get_node("Elemento" + str(i+1) + "/Sprite").set_global_pos(arreglo[i])
		get_node("Elemento" + str(i+1) + "/Polygon2D").set_global_pos(arreglo2[i])
	
	set_fixed_process(true)

func _fixed_process(delta):
	if get_tree().get_nodes_in_group("Conjunto")[0].esta_dentro:
		for i in range(0, 7):
			if i != 0:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[1].esta_dentro:
		for i in range(0, 7):
			if i != 1:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[2].esta_dentro:
		for i in range(0, 7):
			if i != 2:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[3].esta_dentro:
		for i in range(0, 7):
			if i != 3:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[4].esta_dentro:
		for i in range(0, 7):
			if i != 4:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[5].esta_dentro:
		for i in range(0, 7):
			if i != 5:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[6].esta_dentro:
		for i in range(0, 7):
			if i != 6:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	
	if get_tree().get_nodes_in_group("Conjunto")[7].esta_dentro:
		for i in range(0, 7):
			if i != 7:
				get_tree().get_nodes_in_group("Conjunto")[i].esta_dentro = false
	

func shuffleList(list):
	var shuffledList = []
	var indexList = range(list.size())
	for i in range(list.size()):
		randomize()
		var x = randi()%indexList.size()
		shuffledList.append(list[x])
		indexList.remove(x)
		list.remove(x)
	return shuffledList

func _on_BtnVerificar_pressed():
	if (get_tree().get_nodes_in_group("Conjunto")[0].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[1].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[2].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[3].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[4].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[5].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[6].correcto 
		and get_tree().get_nodes_in_group("Conjunto")[7].correcto):
		get_node("Mensaje/Correcto").set_hidden(false)
		global.minijuegos.aplicaciones_sw.juego = true
		global.misiones[2].activa = false
		global.misiones[2].completa = true
		global.misiones[3].activa = true
	else:
		get_node("Mensaje/Incorrecto").set_hidden(false)
		global.minijuegos.aplicaciones_sw.juego = false

func _on_Cerrar_pressed():
	get_node("Mensaje/Incorrecto").set_hidden(true)
	get_node("Mensaje/Correcto").set_hidden(true)
	get_node("Mensaje/Ayuda").set_hidden(true)

func _on_BtnAyuda_pressed():
	get_node("Mensaje/Ayuda").set_hidden(false)

func _on_BtnCerrar_pressed():
	get_tree().change_scene("res://tilesets/SalonClases.tscn")
