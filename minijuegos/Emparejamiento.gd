extends Node2D

var esta_dentro = false
var correcto = false

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if esta_dentro == true:
		get_node("Sprite").set_global_pos(get_global_mouse_pos())

func _on_Area2D_input_event( viewport, event, shape_idx ):
	if event.is_action_pressed("clic_izquierdo"):
		esta_dentro = true
	if event.is_action_released("clic_izquierdo"):
		esta_dentro = false

func _on_AreaSignificado_area_enter( area ):
	var nodo = get_node("Sprite/Area2D")
	if area == nodo:
		correcto = true

func _on_AreaSignificado_area_exit( area ):
	var nodo = get_node("Sprite/Area2D")
	if area == nodo:
		correcto = false
