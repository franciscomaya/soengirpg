extends Control

func _ready():
	get_node("Panel/Opciones").add_item("MISIONES")
	get_node("Panel/Opciones").add_item("CONTROLES")
	
	get_node("Panel/ContainerMisiones/ControlBody").set_bbcode("\n")
	for i in global.misiones:
		if global.misiones[i].activa == true:
			get_node("Panel/ContainerMisiones/ControlBody").append_bbcode("[indent]"+global.misiones[i].mision+"[/indent]\n\n")
			

func _on_Salir_pressed():
	get_tree().set_pause(false)
	get_node(".").queue_free()

func _on_Opciones_item_selected( index ):
	if get_node("Panel/Opciones").get_item_text( index ) == "CONTROLES":
		get_node("Panel/ContainerControles").set_hidden(false)
		get_node("Panel/ContainerMisiones").set_hidden(true)
		get_node("Panel/ContainerSalir").set_hidden(true)
	elif get_node("Panel/Opciones").get_item_text( index ) == "MISIONES":
		get_node("Panel/ContainerControles").set_hidden(true)
		get_node("Panel/ContainerMisiones").set_hidden(false)
		get_node("Panel/ContainerSalir").set_hidden(true)
	#elif get_node("Panel/Opciones").get_item_text( index ) == "SALIR":
	#	get_node("Panel/Opciones").unselect(index)
	#	get_node("Panel/ConfirmSalir").popup()

func _on_ConfirmSalir_confirmed():
	get_tree().quit()
