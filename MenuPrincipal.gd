extends Node2D

func _on_JugarBoton_pressed():
	global.player_position = Vector2(984,576)
	get_tree().change_scene("res://Narracion.tscn")

func _on_SalirBoton_pressed():
	OS.shell_open("http://www.soengirpg.com")
	get_tree().quit()

func _on_Cerrar_pressed():
	get_node("Mensaje/Creditos").set_hidden(true)

func _on_CreditosBoton_pressed():
	get_node("Mensaje/Creditos").set_hidden(false)

