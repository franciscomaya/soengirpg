extends Node2D

var introduccion_res = preload("res://TextoArriba.tscn")
#var tiempo = 0.0
var i = 0
var introduccion
var aparicion = [2, 3.3, 3]
var texto = ["[center]Aquí comienza una historia a través del conocimiento de la Ingeniería de Software.[/center]",
		"[center]Francisco, una persona tranquila, optimista y con muchos ánimos de aventura es el personaje principal \ndurante todo este viaje. Acompáñalo para guiarlo en el camino de convertirse en un experto \nen términos de las Tecnologías de la Información.[/center]",
		"[center]Recorre todos los rincones de los mapas, interactuando con cada uno de los otros personajes \n y así conocer en detalle aspectos relevantes que te permitirán descifrar los enigmas que esconde \nel aprendizaje alrededor de esta temática.[/center]"]

func _ready():
	introduccion = introduccion_res.instance()
	introduccion.set_name("linea"+str(i))
	introduccion.set_pos(Vector2(0, 0))
	get_node(".").add_child(introduccion)
	get_node("linea"+str(i)+"/RichTextLabel").parse_bbcode(texto[i])
	get_node("Timer").set_wait_time(aparicion[i])
	get_node("Timer").start()
	get_node("Timer 2").start()
	i+=1
	set_fixed_process(true)

func _fixed_process(delta):
	#tiempo += delta
	#if( tiempo >= 3 and texto.size()>i ):
	#	var introduccion = introduccion_res.instance()
	#	introduccion.set_name("linea"+str(i))
	#	introduccion.set_pos(Vector2(0, 0))
	#	get_node(".").add_child(introduccion)
	#	get_node("linea"+str(i)+"/RichTextLabel").parse_bbcode(texto[i])
	#	tiempo = 0.0
	#	i+=1
	pass

func _on_Timer_timeout():
	introduccion = introduccion_res.instance()
	introduccion.set_name("linea"+str(i))
	introduccion.set_pos(Vector2(0, 0))
	get_node(".").add_child(introduccion)
	get_node("linea"+str(i)+"/RichTextLabel").parse_bbcode(texto[i])
	i+=1
	if( texto.size()>i ):
		get_node("Timer").set_wait_time(aparicion[i-1])
		get_node("Timer").start()

func _on_Timer_2_timeout():
	get_tree().change_scene("res://tilesets/CasaFrancisco.tscn")
	#print("Fin de la introducción")
