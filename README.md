<p align="center">
    <a href="http://www.soengirpg.com/" target="_blank">
        <img src="http://www.soengirpg.com/sites/default/files/soengi.png" height="70px" />
    </a>
    <h1 align="center">Soengi RPG - <b>So</b>ftware <b>Engi</b>neering <b>R</b>ole <b>P</b>laying <b>G</b>ame</h1>
    <br />
</p>

Soengi RPG es un videojuego de rol para la enseñanza de la Ingeniería de Software, el cual cuenta con un portal web (<a href="http://www.soengirpg.com/" target="_blank">http://www.soengirpg.com</a>) dedicado al apoyo en el proceso colaborativo de desarrollo.

El videojuego se construye mediante la utilización del motor de desarrollo Godot Engine (<a href="https://godotengine.org/" target="_blank">https://godotengine.org</a>) como principal componente, centrando todo su desarrollo en la utilización de herramientas enmarcadas dentro del software libre.

Instalación
------------

- Para su utilización se puede probar importando el proyecto adecuadamente en el motor de desarrollo Godot Engine.
- El entorno de desarrollo contó con un sistema opertativo Ubuntu 16.04
- El ejecutable fue probado en Mozilla Firefox y Google Chrome y se encuentra disponible en el portal <a href="http://www.soengirpg.com/" target="_blank">http://www.soengirpg.com</a>, dedicado a su desarrollo.

Comunidad
---------

- Participa en las [discusiones en el foro](http://www.soengirpg.com/forum/).
- [Chat en IRC](http://www.soengirpg.com/chat/).
- Redes sociales aún no oficiales, pero puedes contactarme en [Facebook](https://www.facebook.com/pachitox/)
- Repositorio en [GitLab](https://gitlab.com/franciscomaya/soengirpg).
- Revisa [otras comunidades](http://www.soengirpg.com/comunidad).

Contribución
------------

El videojuego es [software libre](LICENSE.md) apoyado por la [Universidad Autónoma de Bucaramanga](http://www.unab.edu.co/).

Puedes unirte y:

- [Reportar reportar errores](https://gitlab.com/franciscomaya/soengirpg/issues)
- [Retroalimentarnos con tu experiencia o iniciar una discusión](http://www.soengirpg.com/forum)
- [Contactarnos para contribuir de alguna otra manera](http://www.soengirpg.com/contact)

Estructura de directorios
-------------------

```
addons
    autotile            contiene un complemento para gestión de comportamiento de autotiles
audio
    bgm                 contiene audio de fondo
    se                  contiene audio de efectos especiales
fuentes                 contiene fuente tipográficas
minijuegos              contiene escenas para minijuegos
personajes              contiene texturas y escenas de gestión de personajes
texturas                contiene texturas con sus respectivos tilemaps
tilesets                contiene texturas y escenas de juego
```

### Difunde la voz

Reconocer o citar a Soengi RPG es tan importante como las contribuciones directas.


