extends KinematicBody2D

const SPEED = 100

var anim = ""
var Animacion
var mira = ""
var opciones
var opciones_res

func _ready():
	opciones_res = preload("res://MenuInterno.tscn")
	Animacion = get_node("Animacion")
	#if global.player_position == Vector2(984,576):
	#	opciones = opciones_res.instance()
	#	opciones.set_pos(Vector2(get_node(".").get_pos().x-528, get_node(".").get_pos().y-360))
	#	get_node("/root/").add_child(opciones)
	#	get_tree().set_pause(true)
	set_fixed_process(true)

func _fixed_process(delta):
	var direction = Vector2()
	var new_anim = anim
	
	if Input.is_action_pressed("ui_abajo"):
		direction.y += SPEED
		new_anim = "down"
		mira = "down"
	elif Input.is_action_pressed("ui_arriba"):
		direction.y += -SPEED
		new_anim = "up"
		mira = "up"
	elif Input.is_action_pressed("ui_izquierda"):
		direction.x += -SPEED
		new_anim = "left"
		mira = "left"
	elif Input.is_action_pressed("ui_derecha"):
		direction.x += SPEED
		new_anim = "right"
		mira = "right"
	else:
		new_anim = ""
		Animacion.stop()
	
	if Input.is_action_pressed("ui_opciones"):
		opciones = opciones_res.instance()
		opciones.set_pos(Vector2(get_node(".").get_pos().x-528, get_node(".").get_pos().y-360))
		get_node("/root/").add_child(opciones)
		get_tree().set_pause(true)
		
	if (new_anim != anim):
		anim = new_anim
		Animacion.play(anim)
	
	# set_pos(get_pos() + direction.normalized() * SPEED * delta)
	var motion = direction.normalized() * SPEED * delta
	move(motion)
	
#	if is_colliding():
#		var objeto_x = get_collider_metadata().x
#		var objeto_y = get_collider_metadata().y
#		
#		if(objeto_x == 2 and objeto_y == 5):
#			get_node("Camera2D/CuadroTexto").set_hidden(false)
#			print("Colisión")