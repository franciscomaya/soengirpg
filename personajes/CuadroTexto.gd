extends RichTextLabel

var dialog = []
var page = 0
var cara_ppal

func _ready():
	cara_ppal = preload("res://personajes/person2.png")
	set_process_input(true)
	set_fixed_process(true)

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)

func _input(event):
	if event.type == InputEvent.KEY && event.is_pressed():
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())

func inserta_Dialogo(dialogo):
	page = 0
	dialog = dialogo
	set_bbcode(dialog[page])
	set_visible_characters(0)
	get_node("../Timer").start()

func _fixed_process(delta):
	if page%2 == 0:
		get_node("../CaraParla").set_texture(cara_ppal)
	else:
		get_node("../CaraParla").set_texture(global.cara)
