extends Node2D

var juancarlos = false
var jugador

func _ready():
	var jugador_res = preload("res://personajes/Jugador.tscn")
	jugador = jugador_res.instance()
	jugador.set_pos(global.player_position)
	get_node("Calles/Caminar").add_child(jugador)
	set_fixed_process(true)

func _fixed_process(delta):
	if Input.is_action_pressed("ui_select") and juancarlos == true and jugador.mira == "up" and get_node("Calles/Caminar/Jugador/Camera2D/CuadroTexto").is_hidden() == true:
		var dialogo = []
		global.cara = preload("res://personajes/juancarlos_rostro.png")
		if global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == false:
			dialogo = ["Buen día Ingeniero Juan Carlos, le cuento que en estos días comenzaré mis estudios en Ingeniería de Software, y mis padres creyeron prudente que lo visitara para que me brinde una base de lo que debo tener en cuenta para esta nueva etapa de aprendizaje.",
				"Hola Francisco, ya había escuchado de tí... nuestra ciudad es muy pequeña y las noticias vuelan.\nQuería conocerte pues son muchos los jóvenes que se involucran en el campo de la Tecnología pero son pocos quienes demuestran el talento que me han contado tienes.",
				"Gracias.",
				"Aunque esos son solo rumores y me gustaría ponerte a prueba, además de encomendarte le entregues personalmente una información a un amigo de la ciudadela universitaria.",
				"Jaja, está bien... me gustan los retos.",
				"Por favor, ve al salón de clases de informática y revisa el computador que dejé encendido, allí encontrarás una pequeña actividad... cuando la completes vuelve para acá.",
				"Ok, ya regreso."]
			global.minijuegos.aplicaciones_sw.mensaje = true
			global.misiones[1].activa = false
			global.misiones[1].completa = true
			global.misiones[2].activa = true
			
		elif global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == false:
			dialogo = ["...",
				"No has terminado aún el reto que te puse... apúrate, no tengo todo el día."]
		elif global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == true:
			dialogo = ["Por fín he terminado la actividad... no fue tan sencillo como creía.",
				"Excelente, recuerda lo que aprendiste sobre las aplicaciones del software... en esos conceptos está la clave del enfoque para tus estudios."]
			global.minijuegos.aplicaciones_sw.mensaje = false
			global.misiones[3].activa = false
			global.misiones[3].completa = true
		elif global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == true:
			dialogo = ["Fueron muy valiosos sus aportes ingeniero.",
				"Con mucho gusto... sigue luchando para cumplir tus objetivos."]
		get_node("Calles/Caminar/Jugador/Camera2D/CuadroTexto/RichTextLabel").inserta_Dialogo(dialogo)
		get_node("Calles/Caminar/Jugador/Camera2D/CuadroTexto").set_hidden(false)

func _on_AreaCasaFrancisco_area_enter( area ):
	global.player_position = Vector2(504,648)
	get_tree().change_scene("res://tilesets/CasaFrancisco.tscn")

func _on_AreaColegioFrancisco_area_enter( area ):
	global.player_position = Vector2(624,72)
	get_tree().change_scene("res://tilesets/SalonClases.tscn")

func _on_InteraccionJuanCarlos_area_enter( area ):
	juancarlos = true

func _on_InteraccionJuanCarlos_area_exit( area ):
	juancarlos = false
	get_node("Calles/Caminar/Jugador/Camera2D/CuadroTexto").set_hidden(true)
