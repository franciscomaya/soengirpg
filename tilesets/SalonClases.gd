extends Node2D

var computador = false
var jugador

func _ready():
	var jugador_res = preload("res://personajes/Jugador.tscn")
	jugador = jugador_res.instance()
	jugador.set_pos(global.player_position)
	get_node("Caminar").add_child(jugador)
	
	if global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == false:
		get_node("Infrapuesto/Computador/PCEncendido").set_hidden(false)
	set_fixed_process(true)

func _fixed_process(delta):
	if Input.is_action_pressed("ui_select") and computador == true and jugador.mira == "up" and (global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == false) or (global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == true):
		global.player_position = jugador.get_global_pos()
		get_tree().change_scene("res://minijuegos/Mitos.tscn")

func _on_AreaCiudadFrancisco_area_enter( area ):
	global.player_position = Vector2(-72,216)
	get_tree().change_scene("res://tilesets/CiudadFrancisco.tscn")

func _on_InteraccionPC_area_enter( area ):
	computador = true

func _on_InteraccionPC_area_exit( area ):
	computador = false
