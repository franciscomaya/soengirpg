extends Node2D

var jugador
var cocina = false
var papa = false
var mama = false

func _ready():
	jugador = get_node("Jugador")
	jugador.set_pos(global.player_position)
	set_fixed_process(true)

func _fixed_process(delta):
	if Input.is_action_pressed("ui_select") and cocina == true and get_node("Jugador").mira == "up" and get_node("Jugador/Camera2D/CuadroTexto").is_hidden() == true:
		var dialogo = ["Papá está preparando la comida, todo se ve muy rico."]
		get_node("Jugador/Camera2D/CuadroTexto/RichTextLabel").inserta_Dialogo(dialogo)
		get_node("Jugador/Camera2D/CuadroTexto").set_hidden(false)
	
	if Input.is_action_pressed("ui_select") and papa == true and get_node("Jugador").mira == "up" and get_node("Jugador/Camera2D/CuadroTexto").is_hidden() == true:
		var dialogo = []
		global.cara = preload("res://personajes/papa_rostro.png")
		if global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == false:
			dialogo = ["Hola papá... ¿sabes dónde está mamá?",
				"Ella está en la habitación, estos días ha dormido un poco mas de lo habitual."]
		elif global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == false:
			dialogo = ["...",
				"No debí preparar este plato para el día de hoy."]
		elif global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == true:
			dialogo = ["¿cómo vas?",
				"Se me hizo un poco tarde, te esperamos para comer."]
		elif global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == true:
			dialogo = ["Huele muy bien",
				"Tu madre me 'recomendó' que leyera sobre algo.",
				"Jaja, ella suele ser muy persuasiva. ¿qué leiste?",
				"Algo sobre que adquiriendo las más avanzadas computadoras dispondrá a la gente de mejores herramientas de desarrollo de software.",
				"Jaja, se nota que hiciste la tarea.",
				"Si, creo que tu madre me tendrá haciendo esto por varios días.",]
		get_node("Jugador/Camera2D/CuadroTexto/RichTextLabel").inserta_Dialogo(dialogo)
		get_node("Jugador/Camera2D/CuadroTexto").set_hidden(false)
	
	if Input.is_action_pressed("ui_select") and mama == true and get_node("Jugador").mira == "up" and get_node("Jugador/Camera2D/CuadroTexto").is_hidden() == true:
		var dialogo = []
		global.cara = preload("res://personajes/mama_rostro.png")
		if global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == false:
			dialogo = ["Hola mamá, estoy un poco preocupado... ¿si estaré tomando la decisión correcta al escoger estudiar esta carrera?",
				"Cualquiera sea la decisión que elijas, ten en cuenta que sea tu propia elección... así no tendrás arrepentimiento ninguno. De todas maneras, para que aclares tus dudas podrías visitar al Ing. Juan Carlos y charlar un rato con él."]
		elif global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == false:
			dialogo = ["El ingeniero me envió a realizar una tarea en el salón de informática.",
				"Excelente, escuche que te tiene en buenos conceptos."]
		elif global.minijuegos.aplicaciones_sw.mensaje == true and global.minijuegos.aplicaciones_sw.juego == true:
			dialogo = ["Ya terminé la actividad, no fue tan dificil como creía.",
				"He estado leyendo algo sobre lo que quieres estudiar, cuando hables con el ingeniero ven a verme para contarte lo que aprendí."]
		elif global.minijuegos.aplicaciones_sw.mensaje == false and global.minijuegos.aplicaciones_sw.juego == true:
			dialogo = ["He vuelto, ¿qué querías contarme?",
				"Estuve leyendo sobre mitos del software, creí importante contártelo para que no los olvides.",
				"¡Genial!",
				"Si, los mitos del software propagan información errónea a diferencia de los mitos de la historia que nos dejan una enseñanza.",
				"Ohh, veo que es importante recordar eso",
				"También puse a leer a tu padre sobre algunos mitos del software, jaja. Ve a preguntarle si leyó."]
		get_node("Jugador/Camera2D/CuadroTexto/RichTextLabel").inserta_Dialogo(dialogo)
		get_node("Jugador/Camera2D/CuadroTexto").set_hidden(false)

func _on_Interaccion_area_enter( area ):
	cocina = true

func _on_Interaccion_area_exit( area ):
	cocina = false
	get_node("Jugador/Camera2D/CuadroTexto").set_hidden(true)

func _on_SalidaCasa_area_enter( area ):
	global.player_position = Vector2(600,360)
	get_tree().change_scene("res://tilesets/CiudadFrancisco.tscn")

func _on_InteraccionPapa_area_enter( area ):
	papa = true

func _on_InteraccionMama_area_enter( area ):
	mama = true

func _on_InteraccionPapa_area_exit( area ):
	papa = false
	get_node("Jugador/Camera2D/CuadroTexto").set_hidden(true)

func _on_InteraccionMama_area_exit( area ):
	mama = false
	get_node("Jugador/Camera2D/CuadroTexto").set_hidden(true)
